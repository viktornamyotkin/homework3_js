let num1 = +prompt("Enter your Number 1: ");
while (num1 === '' || isNaN(num1) === true || num1 === null) {
    alert("NOT CORRECT");
    num1 = prompt("Enter your Number again: ", num1);
    if (isNaN(num1) === false && num1 !== '' && num1 !== null) {
        break;
    }
}

let num2 = +prompt("Enter your Number 2: ");
while (num2 === '' || isNaN(num2) === true || num2 === null) {
    alert("NOT CORRECT");
    num2 = prompt("Enter your Number again: ", num2);
    if (isNaN(num2) === false && num2 !== '' && num2 !== null) {
        break;
    }
}
let operation = prompt("Enter the operation you want to do: +, -, /, *");

while (operation !== "+" || operation !== "-" || operation !== "/" || operation !== "*") {
    if (operation === "+" || operation === "-" || operation === "/" || operation === "*") {
        break;
    }
    alert("NOT CORRECT");
    operation = prompt("Enter the operation you want to do: +, -, /, *", operation);
}

function calcOperation(num1, num2) {
    if (operation === "+") {
        return num1 + num2;
    }
    if (operation === "-") {
        return num1 - num2;
    }
    if (operation === "/") {
        return num1 / num2;
    }
    if (operation === "*") {
        return num1 * num2;
    }
}

console.log(calcOperation(num1, num2));

// checkNum(num1);
// checkNum(num2);

// function checkNum(num) {
//     while (num === '' || isNaN(num) === true || num === null) {
//         alert("NOT CORRECT");
//         num = prompt("Enter your Number again: ", num);
//         if (isNaN(num) === false && num !== '' && num !== null) {
//             break;
//         }
//     }
// }

